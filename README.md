# Alphasocket/dockerized-php-fpm-alpine
#### php-fpm-alpine
Dockerized PHP-FPM service on alpine distro

## Branches & Versions
- 7.1
- 7.1-dev
- 7.2
- 7.2-dev
- 7.3
- 7.3-dev
- latest


## Packages installed
- Setup dependencies:
  + gcc
  + py-setuptools
  + openssl-dev
  + jansson-dev
  + python-dev
  + build-base
  + libc-dev
  + file-dev
  + automake
  + autoconf
  + libtool
  + flex
  + linux-headers
  + ghc
  + musl-dev
  + zlib-dev
  + curl
- Runtime dependencies:
  + bash
  + bash-doc
  + bash-completion
  + coreutils
  + man
  + man-pages
  + mdocml-apropos
  + vim
  + grep
  + less
  + less-doc
  + curl
  + zip
  + unzip
  + jq
  + htop
  + git
  + mercurial
  + gnupg
  + fcgi
  + openssh-client
  + ansible
  + file
  + tmux
  + tree
  + xz
  + gzip
  + libxml2
  + libxml2-utils
  + libc6-compat
  + php7
  + php7-curl
  + php7-dom
  + php7-gd
  + php7-ctype
  + php7-gettext
  + php7-iconv
  + php7-json
  + php7-mbstring
  + php7-mcrypt
  + php7-mysqli
  + php7-opcache
  + php7-openssl
  + php7-pdo
  + php7-pdo_dblib
  + php7-pdo_mysql
  + php7-pdo_pgsql
  + php7-pdo_sqlite
  + php7-pear
  + php7-pgsql
  + php7-phar
  + php7-posix
  + php7-session
  + php7-soap
  + php7-sockets
  + php7-sqlite3
  + php7-xml
  + php7-simplexml
  + php7-zip
  + php7-zlib
  + php7-tokenizer
  + php7-xmlwriter
  + nodejs
  + nodejs-npm
  + py2-pip
  + python3
  + mysql-client
  + redis
  + py-crcmod
  + libc6-compat
  + openssh-client
  + nfs-utils
  + iproute2


## Configurable envvars
~~~
CONFIG_VERBOSE="True"
CONFIG_READINESS_TEST="true"
CONFIG_LIVENESS_TEST="true"
CONFIG_PATHS_CONTAINER_STATUS="/tmp/container_status"
CONFIG_GROUPS_ADDITIONAL_ID="1001"
CONFIG_GROUPS_ADDITIONAL_NAME=""
CONFIG_GROUPS_MAIN_NAME="www-data"
CONFIG_GROUPS_MAIN_ID="1082"
CONFIG_USERS_ADDITIONAL_ID="1001"
CONFIG_USERS_ADDITIONAL_NAME=""
CONFIG_USERS_ADDITIONAL_GROUPS=""
CONFIG_USERS_MAIN_NAME="www-data"
CONFIG_USERS_MAIN_ID="1082"
CONFIG_USERS_MAIN_GROUPS="www-data"
CONFIG_PHP_EXT_CURL_ENABLED="True"
CONFIG_PHP_EXT_GD_ENABLED="True"
CONFIG_PHP_EXT_ICONV_ENABLED="True"
CONFIG_PHP_EXT_INTL_ENABLED="False"
CONFIG_PHP_EXT_MYSQLI_ENABLED="True"
CONFIG_PHP_EXT_MBSTRING_ENABLED="True"
CONFIG_PHP_EXT_PDO_MYSQL_ENABLED="True"
CONFIG_PHP_EXT_MCRYPT_ENABLED="True"
CONFIG_PHP_EXT_OPCACHE_ENABLED="True"
CONFIG_PHP_EXT_READLINE_ENABLED="True"
CONFIG_PHP_EXT_REDIS_ENABLED="True"
CONFIG_PHP_EXT_XDEBUG_ENABLED="False"
CONFIG_PHP_EXT_SOAP_ENABLED="True"
CONFIG_PHP_EXT_SODIUM_ENABLED="True"
CONFIG_PHP_EXT_XML_ENABLED="True"
CONFIG_PHP_EXT_ZIP_ENABLED="True"
CONFIG_PHP_PATHS_CONFIG_PHP_FPM_FOLDER="/usr/local/etc/php-fpm.d"
CONFIG_PHP_POOL_NAME="www"
CONFIG_PHP_POOL_USER="www-data"
CONFIG_PHP_POOL_GROUP="www-data"
CONFIG_PHP_POOL_LISTEN_HOST="127.0.0.1"
CONFIG_PHP_POOL_LISTEN_PORT="9000"
~~~
